use std::error::Error;
use clap::Parser;
use std::fs::File;
use std::io::{self, BufRead, BufReader};

type MyResult<T> = Result<T, Box<dyn Error>>;

#[derive(Parser)]
#[command(author, version, about)]
struct Cli {
  #[arg(value_name = "FILE",default_value = "-",help="Input file(s)")]
  files: Vec<String>,
  #[arg(short,long="number",conflicts_with("number_nonblank_lines"),
        help="Number lines")]
  number_lines: bool,
  #[arg(short='b',long="number-nonblank",
        help="Number nonblank lines")]
  number_nonblank_lines: bool,
}

#[derive(Debug)]
pub struct Config {
  files: Vec<String>,
  number_lines: bool,
  number_nonblank_lines: bool,
}

pub fn run(config: Config) -> MyResult<()> {
  for filename in config.files {
    match open(&filename) {
      Err(err) => eprintln!("Failed to open {}: {}", filename, err),
      Ok(br) => {
        let mut count = 0;
        for line_result in br.lines() {
          let line = line_result.unwrap();
          if config.number_lines || config.number_nonblank_lines {
            if config.number_nonblank_lines && line.is_empty() {
              println!("");
              continue;
            } else {
              count += 1;
            }
            println!("{:6}\t{}", count, line);
          } else {
            println!("{}", line);
          }
        }
      },
    }
  }

  Ok(())
}

pub fn get_args() -> MyResult<Config> {
  let cli = Cli::parse();
  Ok(Config {
      files: cli.files,
      number_lines: cli.number_lines,
      number_nonblank_lines: cli.number_nonblank_lines,
  })
}

fn open(filename: &str) -> MyResult<Box<dyn BufRead>> {
  match filename {
    "-" => Ok(Box::new(BufReader::new(io::stdin()))),
    _   => Ok(Box::new(BufReader::new(File::open(filename)?))),
  }
}
