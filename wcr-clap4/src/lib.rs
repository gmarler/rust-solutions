use clap::Parser;
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead, BufReader};

type MyResult<T> = Result<T, Box<dyn Error>>;

#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Cli {
  // Input file(s)
  #[arg(default_value = "-", value_name = "FILE",
        help="Input file(s)")]
  files: Vec<String>,

  // count lines
  #[arg(short('l'), long("lines"),
        help="Show line count")]
  lines: bool,

  // count words
  #[arg(short('w'), long("words"),
        help="Show word count")]
  words: bool,

  // count bytes
  #[arg(short('c'), long("bytes"),
        conflicts_with("chars"),
        help="Show byte count")]
  bytes: bool,

  // count words
  #[arg(short('m'), long("chars"),
        help="Show character count")]
  chars: bool,
}

#[derive(Debug)]
pub struct Config {
  files: Vec<String>,
  lines: bool,
  words: bool,
  bytes: bool,
  chars: bool,
}

#[derive(Debug, PartialEq)]
pub struct FileInfo {
  num_lines: usize,
  num_words: usize,
  num_bytes: usize,
  num_chars: usize,
}

pub fn run(config: Config) -> MyResult<()> {
  let num_files = config.files.len();
  let mut total_lines = 0;
  let mut total_words = 0;
  let mut total_bytes = 0;
  let mut total_chars = 0;

  for filename in &config.files {
    match open(filename) {
      Err(err) => eprintln!("{}: {}", filename, err),
      Ok(fh) => {
        // println!("{:#?}",config);
        let fi = count(fh)?;
        if config.lines { print!("{:>8}", fi.num_lines); }
        if config.words { print!("{:>8}", fi.num_words); }
        if config.bytes { print!("{:>8}", fi.num_bytes); }
        if config.chars { print!("{:>8}", fi.num_chars); }
        if filename != "-" {
          println!(" {}", filename);
        } else {
          println!("");
        }
        if num_files > 1 {
          total_lines += fi.num_lines;
          total_words += fi.num_words;
          total_bytes += fi.num_bytes;
          total_chars += fi.num_chars;
        }
      }
    }
  }
  if num_files > 1 {
    if config.lines { print!("{:>8}", total_lines); }
    if config.words { print!("{:>8}", total_words); }
    if config.bytes { print!("{:>8}", total_bytes); }
    if config.chars { print!("{:>8}", total_chars); }
    println!(" total");
  }

  Ok(())
}

pub fn get_args() -> MyResult<Config> {
  let mut cli = Cli::parse();

  if [ cli.lines, cli.words, cli.bytes, cli.chars ]
      .iter()
      .all(|v| v == &false) {
    cli.lines = true;
    cli.words = true;
    cli.bytes = true;
  }

  Ok(Config {
      files: cli.files,
      lines: cli.lines,
      words: cli.words,
      bytes: cli.bytes,
      chars: cli.chars,
  })
}

pub fn count(mut file: impl BufRead) -> MyResult<FileInfo> {
  let mut num_lines = 0;
  let mut num_words = 0;
  let mut num_bytes = 0;
  let mut num_chars = 0;

  let mut buf = String::new();
  loop {
    let bytes_read = file.read_line(&mut buf)?;
    if bytes_read == 0 {
      break;
    }
    num_lines += 1;
    num_words += buf.split_whitespace().count();
    num_bytes += bytes_read;
    num_chars += buf.chars().count();
    buf.clear();
  }

  Ok(FileInfo {
      num_lines,
      num_words,
      num_bytes,
      num_chars,
  })
}

fn open(filename: &str) -> MyResult<Box<dyn BufRead>> {
  match filename {
    "-" => Ok(Box::new(BufReader::new(io::stdin()))),
    _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
  }
}

#[cfg(test)]
mod tests {
  use super::{count, FileInfo};
  use std::io::Cursor;

  #[test]
  fn test_count() {
    let text = "I don't want the world. I just want your half.\r\n";
    let info = count(Cursor::new(text));
    assert!(info.is_ok());
    let expected = FileInfo {
      num_lines: 1,
      num_words: 10,
      num_chars: 48,
      num_bytes: 48,
    };
    assert_eq!(info.unwrap(), expected);
  }
}

