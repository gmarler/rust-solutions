use std::error::Error;
use clap::{App, Arg};
use std::fs::File;
use std::io::{self, BufRead, BufReader};

type MyResult<T> = Result<T, Box<dyn Error>>;

#[derive(Debug)]
pub struct Config {
    files: Vec<String>,
    number_lines: bool,
    number_nonblank_lines: bool,
}

pub fn run(config: Config) -> MyResult<()> {
  for filename in config.files {
    match open(&filename) {
      Err(err) => eprintln!("Failed to open {}: {}", filename, err),
      Ok(fhandle) => {
        let mut nb_line_count = 0;
        for (line_num, line) in fhandle.lines().enumerate() {
          let line = line?;
          if config.number_lines {
            println!("{:>6}\t{}", line_num + 1, line);
          } else if config.number_nonblank_lines {
            if !line.is_empty() {
              nb_line_count += 1;
              println!("{:>6}\t{}", nb_line_count, line);
            } else {
              println!();
            }
          } else {
            println!("{}", line);
          }
        }
      }
    }
  }
  Ok(())
}

fn open(filename: &str) -> MyResult<Box<dyn BufRead>> {
    match filename {
        "-" => Ok(Box::new(BufReader::new(io::stdin()))),
        _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
    }
}

pub fn get_args() -> MyResult<Config> {
    let matches = App::new("catr")
        .version("0.1.0")
        .author("Gordon Marler <gmarler@gmarler.com>")
        .about("Rust cat")
        .arg(
            Arg::with_name("files")
            .value_name("FILE")
            .default_value("-")
            .multiple(true)
            .help("Input file(s)"),
            )
        .arg(
            Arg::with_name("number")
            .long("number")
            .short("n")
            .conflicts_with("number_nonblank")
            .help("Number lines")
            .takes_value(false),
            )
        .arg(
            Arg::with_name("number_nonblank")
            .long("number-nonblank")
            .short("b")
            .help("Number non-blank lines")
            .takes_value(false),
            )
        .get_matches();

    Ok(Config {
        files: matches.values_of_lossy("files").unwrap(),
        number_lines: matches.is_present("number"),
        number_nonblank_lines: matches.is_present("number_nonblank"),
    })
}
