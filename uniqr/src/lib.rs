use clap::{App, Arg};
use std::error::Error;

type MyResult<T> = Result<T, Box<dyn Error>>;

#[derive(Debug)]
pub struct Config {
  in_file: String,
  out_file: Option<String>,
  count: bool,
}

pub fn get_args() -> MyResult<Config> {
  let matches = App::new("uniqr")
    .version("0.1.0")
    .author("Gordon Marler <gmarler@bloomberg.com>")
    .about("Rust uniq")
    // Add here
    .arg(
      Arg::with_name("in_file")
      .value_name("IN_FILE")
      .default_value("-")
      .help("Input file"),
    )
    .arg(
      Arg::with_name("count")
      .long("count")
      .short("c")
      .help("Show counts"),
    )
    .arg(
      Arg::with_name("out_file")
      .value_name("OUT_FILE")
      .help("Output file"),
    )
    .get_matches();

  Ok(Config {
    in_file: matches.value_of_lossy("in_file").unwrap().to_string(),
    out_file: matches.value_of("out_file").map(String::from),
    count: matches.is_present("count")
  })
}

pub fn run(config: Config) -> MyResult<()> {
  println!("{:?}", config);
  Ok(())
}
