use clap::Parser;
use std::error::Error;
use std::fs::File;
use std::io::{self, Read, BufRead, BufReader};

type MyResult<T> = Result<T, Box<dyn Error>>;

#[derive(Parser,Debug)]
#[command(author, version, about)]
struct Cli {
  #[arg(value_name="FILE",default_value = "-",
         help="Input file(s)")]
  files: Vec<String>,
  #[arg(short='n',long="lines",default_value = "10",
        value_name="LINES",
        conflicts_with("bytes"),
        value_parser = clap::value_parser!(u64).range(1..),
        help="Number of lines")]
  lines: u64,
  #[arg(short='c',long="bytes",
        value_name="BYTES",
        value_parser = clap::value_parser!(u64).range(1..),
        help="Number of bytes")]
  bytes: Option<u64>,
}

#[derive(Debug)]
pub struct Config {
  files: Vec<String>,
  lines: u64,
  bytes: Option<u64>,
}

pub fn run(config: Config) -> MyResult<()> {
  let num_files = config.files.len();

  for (file_num, filename) in config.files.iter().enumerate() {
    match open(&filename) {
      Err(err) => eprintln!("{}: {}", filename, err),
      Ok(mut file) => {
        if num_files > 1 {
          println!(
            "{}==> {} <==",
            if file_num > 0 { "\n" } else { "" },
            filename
          );
        }
        if let Some(num_bytes) = config.bytes {
          let mut handle = file.take(num_bytes);
          let mut buffer = vec![0; num_bytes as usize];
          let bytes_read = handle.read(&mut buffer)?;
          print!("{}",
                 String::from_utf8_lossy(&buffer[..bytes_read]));
        } else { // Do by lines, not bytes
          let mut line = String::new();
          for _ in 0..config.lines {
            let bytes = file.read_line(&mut line)?;
            if bytes == 0 {
              break;
            }
            print!("{}",line);
            line.clear();
          }
        }
      },
    }
  }
  Ok(())
}

pub fn get_args() -> MyResult<Config> {
  let cli = Cli::parse();
  Ok(Config {
      files: cli.files,
      lines: cli.lines,
      bytes: cli.bytes,
  })
}

fn open(filename: &str) -> MyResult<Box<dyn BufRead>> {
  match filename {
    "-" => Ok(Box::new(BufReader::new(io::stdin()))),
    _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
  }
}
