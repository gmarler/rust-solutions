use clap::{App, Arg};
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Read};

type MyResult<T> = Result<T, Box<dyn Error>>;

#[derive(Debug)]
pub struct Config {
  files: Vec<String>,
  lines: usize,
  bytes: Option<usize>,
}

fn parse_positive_int(val: &str) -> MyResult<usize> {
  match val.parse() {
    Ok(n) if n > 0 => Ok(n),
    _ => Err(From::from(val)),
  }
}

#[test]
fn test_parse_positive_int() {
  // 3 is an OK integer
  let res = parse_positive_int("3");
  assert!(res.is_ok());
  assert_eq!(res.unwrap(), 3);

  // Any string is an error
  let res = parse_positive_int("foo");
  assert!(res.is_err());
  assert_eq!(res.unwrap_err().to_string(), "foo".to_string());

  // A zero is an error
  let res = parse_positive_int("0");
  assert!(res.is_err());
  assert_eq!(res.unwrap_err().to_string(), "0".to_string());
}

pub fn run(config: Config) -> MyResult<()> {
  let num_files = config.files.len();

  for (file_num, filename) in config.files.iter().enumerate() {
    match open(&filename) {
      Err(err) => eprintln!("Failed to open {}: {}", filename, err),
      Ok(mut fhandle) => {
        if num_files > 1 {
          println!(
            "{}==> {} <==",
            if file_num > 0 { "\n" } else { "" },
            filename
          );
        }
        if config.bytes.is_none() {
          let mut line = String::new();
          for _ in 0..config.lines {
            let bytes = fhandle.read_line(&mut line)?;
            if bytes == 0 {
              break;
            }
            print!("{}", line);
            line.clear();
          }
        } else {
          let num_bytes = config.bytes.unwrap();
          let bytes: Result<Vec<_>, _> = fhandle.bytes().take(num_bytes).collect();
          print!("{}", String::from_utf8_lossy(&bytes?));
        }
      }
    }
  }
  Ok(())
}

fn open(filename: &str) -> MyResult<Box<dyn BufRead>> {
    match filename {
        "-" => Ok(Box::new(BufReader::new(io::stdin()))),
        _ => Ok(Box::new(BufReader::new(File::open(filename)?))),
    }
}

pub fn get_args() -> MyResult<Config> {
  let matches = App::new("headr")
    .version("0.1.0")
    .author("Gordon Marler <gmarler@bloomberg.net>")
    .about("Rust head")
    .arg(
      Arg::with_name("files")
      .value_name("FILE")
      .default_value("-")
      .multiple(true)
      .help("Input file(s)"),
    )
    .arg(
      Arg::with_name("lines")
      .long("lines")
      .short("n")
      .value_name("LINES")
      .default_value("10")
      .help("Number of lines")
      .takes_value(true),
    )
    .arg(
      Arg::with_name("bytes")
      .long("bytes")
      .short("c")
      .value_name("BYTES")
      .conflicts_with("lines")
      .help("Number of bytes")
      .takes_value(true),
    )
    .get_matches();

  let lines = matches
    .value_of("lines")
    .map(parse_positive_int)
    .transpose()
    .map_err(|e| format!("illegal line count -- {}", e))?;

  let bytes = matches
    .value_of("bytes")
    .map(parse_positive_int)
    .transpose()
    .map_err(|e| format!("illegal byte count -- {}", e))?;

  Ok(Config {
    files: matches.values_of_lossy("files").unwrap(),
    lines: lines.unwrap(),
    bytes,
  })
}

